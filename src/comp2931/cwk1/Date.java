// Class for COMP2931 Coursework 1
package comp2931.cwk1;
import java.lang.Object;
import java.util.GregorianCalendar;
import java.util.Calendar;

/**
 * Simple representation of a date.
 */
public class Date {

  private int year;
  private int month;
  private int day;
  private final int feb = 2;
  int[] month31 = new int[]{1,3,5,7,8,10,12};
  int[] month30 = new int[]{4,6,9,11};

  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
    if(999<y && y<9999) {
      year = y;
    } else {
      throw new IllegalArgumentException("Incorrect value for year. (4 digits only)");
    }
    if(0<m && m<13) {
      month = m;
    } else {
      throw new IllegalArgumentException("Incorrect value for month. (1 - 12)");
    }

    if(find(month31, m) && (0<d && d<32)){
      day = d;
    } else if(find(month30, m) && (0<d && d<31)){
      day = d;
    } else if((m == feb) && (0<d && d<29) && !isLeapYear(year)){
      day = d;
    } else if((m == feb) && (0<d && d<30) && isLeapYear(year)){
      day = d;
    } else {
      throw new IllegalArgumentException("Incorrect value for day. HINT: Check number of days in chosen month.");
    }
  }

  /**
   *Creates a date object using today's date via the Calender class.
   */
  public Date(){
    Calendar today = Calendar.getInstance();
    year = today.get(Calendar.YEAR);
    //+1 because MONTH starts at 00
    month = today.get(Calendar.MONTH)+1;
    day = today.get(Calendar.DAY_OF_MONTH);
  }

  /**
   * Checks that the passed integer is in the passed int array.
   *
   * @param array Array being iterated through
   * @param n Integer
   * @return Boolean
   */
  public boolean find(int[] array, int n){
    for(int i:array){
      if(n == i){
        return true;
      }
    }
    return false;
  }

  /**
   * Using a Date object, the method finds what day of
   * the year the object is.
   *
   * @return Day of year as an int value
   */
  public int getDayOfYear(){
    Calendar date = new GregorianCalendar();
    //month-1 because the days in the specified month are counted as well
    date.set(year, (month-1), day);
    return date.get(Calendar.DAY_OF_YEAR);
  }

  /**
   *Checks whether the passed year(int) is a leap year or not using the
   *calendar class.
   *
   * @param yr Year
   * @return Boolean
   */
  public boolean isLeapYear(int yr){
    Calendar date = new GregorianCalendar();
    date.set(Calendar.YEAR, yr);
    return date.getActualMaximum(Calendar.DAY_OF_YEAR)==366;
  }

  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%2d", year, month, day);
  }

  /**
   * Compares objects checking if they're the same Date object or
   * have the same values.
   *
   * @param other Other date for comparison
   * @return Boolean
   */
  @Override
  public boolean equals(Object other){
    if(other == this){
      return true;
    } else if(!(other instanceof Date)){
      return false;
    } else{
      Date otherDate = (Date) other;
      return getDay() == otherDate.getDay()
              && getMonth() == otherDate.getMonth()
              && getYear() == otherDate.getYear();
    }
  }
}
