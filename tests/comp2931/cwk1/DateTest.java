package comp2931.cwk1;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import java.util.Calendar;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Date.class})

public class DateTest {

    private Date dateOne;
    private Date dateTwo;
    private Date dateThree;

    @Before
    public void setUp() throws Exception {
        dateOne = new Date(2017,10,12);
        dateTwo = new Date(2017, 2, 20);
        dateThree = new Date(2017, 1,1);
    }

    @Test
    public void testDateFormat() throws Exception {

        assertThat(dateOne.getDay(), is(12));
        assertThat(dateOne.getMonth(), is(10));
        assertThat(dateOne.getYear(), is(2017));
        assertThat(dateOne.toString(), is("2017-10-12"));

        assertThat(dateTwo.getDay(), is(20));
        assertThat(dateTwo.getMonth(), is(2));
        assertThat(dateTwo.getYear(), is(2017));
        assertThat(dateTwo.toString(), is("2017-02-20"));
    }

    @Test
    public void mockDate() {
        Calendar test = Calendar.getInstance();
        test.set(2017, Calendar.NOVEMBER, 7 );
        PowerMockito.mockStatic(Calendar.class);
        Mockito.when(Calendar.getInstance()).thenReturn(test);

        Date testDate = new Date();

        assertThat(testDate.getYear(), is(2017));
        assertThat(testDate.getMonth(), is(11));
        assertThat(testDate.getDay(), is(7));
    }

    @Test(expected = IllegalArgumentException.class)
    public void monthTooHigh(){new Date(2017, 14, 25);}

    @Test(expected = IllegalArgumentException.class)
    public void monthTooLow(){new Date(2017, -10, 25);}

    @Test(expected = IllegalArgumentException.class)
    public void dayTooHigh(){new Date(2017, 10, 35);}

    @Test(expected = IllegalArgumentException.class)
    public void dayTooHighFeb(){new Date(2017, 2, 30);}

    @Test(expected = IllegalArgumentException.class)
    public void dayTooLow(){new Date(2017, 10, 0);}

    @Test(expected = IllegalArgumentException.class)
    public void yearTooLow(){new Date(17,5,9);}

    @Test(expected = IllegalArgumentException.class)
    public void yearTooHigh(){new Date(20017,5,9);}

    @Test
    public void equality(){
        assertThat(dateOne.equals(new Date(2017,10,12)), is(true));
        assertThat(dateTwo.equals(new Date(2017, 2,20)), is(true));
        assertThat(dateOne.equals(dateTwo), is(false));
    }

    @Test
    public void dayOfYear(){
        assertThat(dateThree.getDayOfYear(), is(1));
        assertThat(dateOne.getDayOfYear(), is(285));
    }

    @Test
    public void leapYear(){new Date(2000, 2,29);}

    @Test(expected = IllegalArgumentException.class)
    public void notLeapYear(){new Date(2017, 2, 29);}
}